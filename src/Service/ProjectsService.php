<?php declare(strict_types=1);

namespace Gavrecky\Gitlab\Service;

use Gavrecky\Gitlab\Exception\Runtime\ResponseException;
use Gavrecky\Gitlab\Project\ProjectsClient;
use Nette\Utils\Json;
use Nette\Utils\JsonException;
use Psr\Http\Message\ResponseInterface;

final class ProjectsService extends AbstractService
{

	/** @var ProjectsClient */
	protected $client;

	public function __construct(ProjectsClient $client)
	{
		$this->client = $client;
	}


	public function getAllProjects(array $attributtes = []): array
	{
		$response = $this->client->all($attributtes);
		$projects = $this->parseProjects($response);

		return $projects;
	}

	private function parseProjects(ResponseInterface $response): array
	{
		if ($response->getStatusCode() !== 200) {
			throw new ResponseException(
				$response,
				sprintf('Server responded with status code "%d"', $response->getStatusCode())
			);
		}

		try {
			$data = Json::decode($response->getBody()->getContents());
		} catch (JsonException $e) {
			throw new ResponseException($response, 'Cannot decode response json');
		}

		return $data;
	}

}
