<?php declare(strict_types = 1);

namespace Gavrecky\Gitlab\Service;

use Gavrecky\Gitlab\Exception\Runtime\ResponseException;
use Psr\Http\Message\ResponseInterface;

abstract class AbstractService
{

	/**
	 * @param ResponseInterface $response
	 * @param int[]             $allowedStatusCodes
	 */
	protected function assertResponse(ResponseInterface $response, array $allowedStatusCodes = [200]): void
	{
		if (!in_array($response->getStatusCode(), $allowedStatusCodes, true)) {
			throw new ResponseException($response);
		}
	}

}
