<?php declare(strict_types=1);

namespace Gavrecky\Gitlab\Http;

use Psr\Http\Message\ResponseInterface;

interface HttpClient
{

	/**
	 * @param string  $method
	 * @param string  $uri
	 * @param mixed[] $options
	 * @return ResponseInterface
	 */
	public function request(string $method, string $uri, array $options = []): ResponseInterface;
}
