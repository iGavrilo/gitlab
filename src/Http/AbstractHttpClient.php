<?php declare(strict_types = 1);

namespace Gavrecky\Gitlab\Http;

abstract class AbstractHttpClient
{

	const REQUEST_TIMEOUT = 5;

	/** @var mixed[] */
	protected $config = [];

	/** @var HttpClient */
	protected $httpClient;

	/**
	 * @param HttpClient $httpClient
	 * @param mixed[]    $config
	 */
	public function __construct(HttpClient $httpClient, array $config)
	{
		$this->httpClient = $httpClient;
		$this->config = $config;
	}
}
