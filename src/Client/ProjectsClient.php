<?php declare(strict_types=1);

namespace Gavrecky\Gitlab\Project;

use Gavrecky\Gitlab\Http\HttpClient;
use Gavrecky\Gitlab\Http\AbstractHttpClient;
use Psr\Http\Message\ResponseInterface;

class ProjectsClient extends AbstractHttpClient
{

	const PROJECTS_ENDPOINT = 'projects';

	public function __construct(HttpClient $httpClient, array $config)
	{
		parent::__construct($httpClient, $config);
	}

	public function all(array $attributes = []): ResponseInterface
	{
		return $this->httpClient->request('GET', self::PROJECTS_ENDPOINT , ['query' => $attributes]);
	}

	public function single(int $projectId, array $attributes = []): ResponseInterface
	{
		return $this->httpClient->request('GET', sprintf('%s/' . $projectId, self::PROJECTS_ENDPOINT), ['query' => $attributes]);
	}

	public function pushRulesGet(int $projectId): ResponseInterface
	{
		return $this->httpClient->request('GET', sprintf('%s/' . $projectId . '/push_rule', self::PROJECTS_ENDPOINT));
	}

	public function pushRulesAdd(int $projectId, array $attributes = []): ResponseInterface
	{
		return $this->httpClient->request('POST', sprintf('%s/' . $projectId . '/push_rule', self::PROJECTS_ENDPOINT), ['query' => $attributes]);
	}

	public function pushRulesEdit(int $projectId, array $attributes = []): ResponseInterface
	{
		return $this->httpClient->request('PUT', sprintf('%s/' . $projectId . '/push_rule', self::PROJECTS_ENDPOINT), ['query' => $attributes]);
	}

	public function pushRulesDelete(int $projectId): ResponseInterface
	{
		return $this->httpClient->request('PUT', sprintf('%s/' . $projectId . '/push_rule', self::PROJECTS_ENDPOINT));
	}


}
