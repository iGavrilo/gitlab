<?php declare(strict_types = 1);

namespace Gavrecky\Gitlab\Exception\Runtime;

use Gavrecky\Gitlab\Exception\RuntimeException;

class RequestException extends RuntimeException
{

}
