<?php declare(strict_types = 1);

namespace Gavrecky\Gitlab\Exception\Logical;

use Gavrecky\Gitlab\Exception\Exception\LogicalException;

class InvalidStateException extends LogicalException
{

}
