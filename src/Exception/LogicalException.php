<?php declare(strict_types = 1);

namespace Gavrecky\Gitlab\Exception\Exception;

use LogicException;

class LogicalException extends LogicException
{

}
