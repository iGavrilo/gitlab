<?php declare(strict_types=1);

namespace Gavrecky\Gitlab\Bridges\DI\Nette;

use Gavrecky\Gitlab\Http\GuzzleClient;
use Gavrecky\Gitlab\Http\HttpClient;
use Gavrecky\Gitlab\Project\ProjectsClient;
use Gavrecky\Gitlab\Service\ProjectsService;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use Nette\DI\CompilerExtension;
use Nette\DI\ContainerBuilder;

/**
 * GitlabApiExtension
 */
class GitlabApiExtension extends CompilerExtension
{

	/** @var mixed[] */
	protected $defaults = [
		'http' => [
			'base_uri' => 'https://gitlab.com/api/v4/',
		],
	];

	/**
	 * Register services
	 */
	public function loadConfiguration()
	{
		$config = $this->validateConfig($this->defaults);

		/** @var ContainerBuilder $builder */
		$builder = $this->getContainerBuilder();

		$builder->addDefinition($this->prefix('guzzle.client'))
			->setFactory(Client::class, [$config['http']])
			->setType(ClientInterface::class)
			->setAutowired(false);

		$builder->addDefinition($this->prefix('http.client'))
			->setFactory(GuzzleClient::class, [$this->prefix('@guzzle.client')])
			->setType(HttpClient::class)
			->setAutowired(false);

		$builder->addDefinition($this->prefix('client.projects'))
			->setFactory(ProjectsClient::class, [$this->prefix('@http.client'), $config]);

		$builder->addDefinition($this->prefix('service.projects'))
			->setFactory(ProjectsService::class, [$this->prefix('@client.projects')]);


	}
}
